import { Component } from '@angular/core';
import {MatDialog} from '@angular/material';
import { MyDialogComponent } from './my-dialog/my-dialog.component';
import { DialogData } from './DialogData';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  data: DialogData = {name: 'Vasya', animal: 'Dog'};

  constructor(public matDialogService: MatDialog) {}

  openDialog() {
    this.matDialogService.open(MyDialogComponent, {
      width: '1200px',
      data: {data: this.data},
      panelClass: 'my-dialog'
    });
  }
}
