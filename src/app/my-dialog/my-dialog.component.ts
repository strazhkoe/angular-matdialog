import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {DialogData} from '../DialogData';


@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MyDialogComponent implements OnInit {

  menuInfo: MenuInfoItem[] = [
    new MenuInfoItem('First', 'First'),
    new MenuInfoItem('Second', 'Second'),
    new MenuInfoItem('Third', 'Third')
  ];

  constructor(
    dialogRef: MatDialogRef<MyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() {
  }

  menuSelected(menuName: string): void {
    
  }

}

export class MenuInfoItem {
  constructor(
    public menuName: string,
    public menuComponent: Object) {}
}
